package com.pinewoodbuilders.contracts.oauth;

public record XeusOAuthRequest(Long discordId, Long robloxId, String state) {}
